class TweetsController < ApplicationController
  #before_action :finder, only: [:edit, :update, :destroy]
  
  #def finder
   # @tweet = Tweet.find(params[:id])
  #end
  
  
  
  def index
  end
 
  def show 
       @tweet = Tweet.find(params[:id])

  end
  
  def new 
    @tweet = Tweet.new
  end
  
  def create
    puts "###########"
    puts params
    puts "###########"
   
    @zombie = Zombie.find(params[:tweet][:zombie_id])
    @tweet = @zombie.tweets.create(params.require(:tweet).permit(:zombie_id, :status)) 
    
    respond_to do |format|
      if @tweet.save
        format.html { redirect_to @zombie, notice: 'створено новий твіт' }
        format.json { render action: 'show', status: :created, location: @tweet }
        format.js 
      else
        format.html { render action: 'new' }
        format.json { render json: @tweet.errors, status: :unprocessable_entity }
      end
    end
    
    
  end
  
  def edit 
    @tweet = Tweet.find(params[:id])
  end
  
  def update 
   @tweet = Tweet.find(params[:id])
    respond_to do |format|
      if @tweet.update(params.require(:tweet).permit(:zombie_id, :status))
        format.html { redirect_to @tweet, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @tweet.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy 
    @tweet = Tweet.find(params[:id])
    @tweet.destroy
    respond_to do |format|
      format.html { redirect_to tweets_url }
      format.json { head :no_content }
    end
  end


end

